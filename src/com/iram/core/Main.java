package com.iram.core;

public class Main {

	public static void main(String[] args) {
	
            System.out.println(testN1(convierteARomano(1)));
            System.out.println(testN2(convierteARomano(2)));
            System.out.println(testN3(convierteARomano(3)));
            System.out.println(testN8(convierteARomano(8)));
	}
	
	public static String convierteARomano(int num) {
		
		 switch(num)
		{
		  case 1: return "I";
		  case 2: return "II";
		  case 3: return "III";
		
		  default: return "Error";
		}
		
		/*if(num ==1)
			return "I";
		else if(num == 2)
			return "II";
		else if(num == 3)
			return "III";
		else
			return "Error";*/
		
		//return "";
	}
	
	public static Boolean testN1(String result) {
		if(result.equals("I"))
			return true;
		return false;
	}
	
	public static Boolean testN2(String result) {
		if(result.equals("II"))
			return true;
		return false;
	}
	
	public static Boolean testN3(String result) {
		if(result.equals("III"))
			return true;
		return false;
	}
	
	public static Boolean testN8(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}

}
